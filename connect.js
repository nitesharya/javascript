function Student(){
    this.title = "Mr.";
    this.name = "Steve";
  }
  
  var student1 = new Student();
  
  Object.defineProperty(student1,'fullName',{
    get:function(){
      return this.title + ' ' + this.name;
    },
    set:function(_fullName){
      this.title = _fullName.split(' ')[0];
      this.name = _fullName.split(' ')[1];
    }
  });
  
  student1.fullName = "Mr.  John";
  
  console.log(student1.title);
  console.log(student1.name);