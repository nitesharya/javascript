$('document').ready(function(){
    $('#signupform').on('submit', function (e) {
        e.preventDefault();
        
        //alert('res');    
        $.ajax({
            url: 'register.php',
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType:false,
            cache:false,
            async: false,
            success: function (res) {
                alert(res);
            },
            error: function () {
                alert('something went to wrong');
            }
            
        });

    });

    ////////////////login-form/////////////////
    $('#loginform').on('submit', function (e) {
        e.preventDefault();
        
        //alert('res');    
        $.ajax({
            url: 'login.php',
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (res) {
                if (res == 1) {
                    window.location = 'portfolio.php';
                   
                }
                else {
                    alert(res);
                }
            },
            error: function () {
                alert('something went to wrong');
            }
            
        });
    });

        //message form
        $('#messageform').on('submit', function (e) {
            e.preventDefault();
            
            //alert('res');    
            $.ajax({
                url: 'message.php',
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function (res) {
                    if (res == 1) {
                        alert("your message had been sent thankyou");
                       
                    }
                    else {
                        alert(res);
                    }
                },
                error: function () {
                    alert('something went to wrong');
                }
                
            });

        });
    

    });