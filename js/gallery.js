
    $(document).ready(function(){
     
        $('.lightbox').click(function(){
          
		var title = $(this).attr('title');
		var src = $(this).children('img').attr("src").replace('/240/140/', '/480/280/');
		// Change the line above to modify the src according to your naming convention for larger images. 
		// You could even change it to source a data attrib ;)

        var alt = $(this).children('img').attr("alt") || "";
    
            var $img = $('<img class="center-block img-responsive" alt="' + alt + '" src="' + src + '">');
            
            $('.modal-title').html(title);
            
		$('.modalbody').html('<p class="text-danger">Loading...</p>');
		$('#lightbox').modal({
				show: true
        });
        $('.modalbody').html($img);
           
    });
  
        
    });
 